import { Component, ViewChild, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { TooltipDirective } from './tooltip.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'xello-interview-project';
  @ViewChild(TooltipDirective)

  public tooltipDir: TooltipDirective;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.tooltipDir.hideAll();
  }
  constructor(private renderer: Renderer2) {
    this.renderer.listen('window', 'click', (e: Event) => {
    const data = this.tooltipDir.getElementsByIdStartsWith('tooltip', 'Button', 'button_');
    let check = 0;
    for ( let i = 0; i < data.length; i++) {
      if (data[i] === e.target ) {
        check = 1;
      }
    }
    if (check !== 1) {
      this.tooltipDir.hideAll();
    }
    });
  }


}
