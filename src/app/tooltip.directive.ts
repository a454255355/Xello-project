import { Directive, Input, ElementRef, HostListener, Renderer2, ViewChild } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[tooltip]'
})
export class TooltipDirective {
  @Input('tooltip') tooltipTitle: string;
  @Input() placement: string;
  tooltip: HTMLElement;
  target: HTMLElement;
  main = document.getElementsByClassName('tooltip-example');
  offset = 10;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('click', ['$event.target']) onclick(eventTarget: any) {
    this.target = eventTarget;
    this.toggle(this.target);

  }
  toggle(eventTarget: any) {
    this.target = eventTarget;
    // console.log(this.target.parentNode.childNodes);
    let count = 0;
    for (let i = 0; i < this.target.parentNode.childElementCount; i++) {
      if (this.target.parentNode.children[i].classList.contains('ng-tooltip-show')) {
        count++;
      }
    }
    if (count > 0) {
      this.target = this.target.parentElement;
      this.hide();
      this.hideAll();
    } else {
      this.target = this.target.parentElement;
      this.hideAll();
      this.show();
    }

  }

  show() {
    this.create();
    this.setPosition();
    this.renderer.addClass(this.tooltip, 'ng-tooltip-show');
  }

  hideAll() {
    for ( let i = 0; i < this.main.item(0).childElementCount; i++) {
      if ( this.main.item(0).children[i].children[1]) {
        this.renderer.removeChild(this.main.item(0).children[i], this.main.item(0).children[i].children[1]);
      }
    }
  }

  hide() {
    console.log(1111);
    console.log(this.tooltip);
    console.log(this.tooltip.parentNode);
    if (this.tooltip) {
      this.renderer.removeClass(this.tooltip.parentNode, 'ng-tooltip-show');
      this.renderer.removeChild(this.target, this.tooltip);
      this.tooltip = null;
    }
  }

  create() {
    this.tooltip = this.renderer.createElement('span');
    this.renderer.appendChild(
      this.tooltip,
      this.renderer.createText(this.tooltipTitle) // textNode
    );
    this.renderer.appendChild(this.target, this.tooltip);
    this.renderer.addClass(this.tooltip, 'ng-tooltip');
    this.renderer.addClass(this.tooltip, `ng-tooltip-${this.placement}`);
  }

  getElementsByIdStartsWith(container, selectorTag, prefix) {
    const items = [];
    const datas = document.getElementById(container).getElementsByTagName(selectorTag);
    for (let i = 0; i < datas.length; i++) {
        if (datas[i].id.lastIndexOf(prefix, 0) === 0) {
            items.push(datas[i]);
        }
    }
    return items;
}

  setPosition() {
    this.renderer.addClass(this.tooltip, 'col-lg-12');
    this.renderer.addClass(this.tooltip, 'col-md-12');
    this.renderer.addClass(this.tooltip, 'col-sm-6');
    this.renderer.addClass(this.tooltip, 'col-xs-6');
    this.renderer.addClass(this.tooltip, 'ml-2');

  }
}
